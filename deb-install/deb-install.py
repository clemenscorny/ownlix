#!/usr/bin/env python3

import sys
import json
import subprocess

def install(path='packages.json'):
    """Install debian packages

    Keyword arguments:
    path -- path to the .json file (default 'packages.json')
    """

    with open(path, 'r') as json_file:
        packages = json.load(json_file)

        # update package list
        print('\nUpdate\n======\n')
        cmd = ['sudo', 'apt-get', 'update']
        print('run:', ' '.join(cmd))
        subprocess.call(cmd)

        print('\nInstall packages\n================\n')
        cmd = ['sudo', 'apt-get', 'install', '-y']
        # add package names to list
        cmd += packages.keys()
        print('run:', ' '.join(cmd))

        subprocess.call(cmd)

if __name__ == '__main__':
    # use 1 argument as path to the .json file if provided
    if len(sys.argv) > 1:
        install(sys.argv[1])
    else:
        install()
