# deb-install
This program installs programs with the debian package manager

## Configuration
The packages are defined in `packages.json`.

There are two possibilities to define these packages.

1. standard package (can be found in the default sources)  
   "package_name": "package_information"
2. personal package archive.  
   "package_name": ["package_information", "ppa:<user>/<ppa-name>"]

Example
```json
{
    "vlc": "multimedia player and streamer",
    "atom": ["Atom is a hackable text editor for the 21st century, developed by GitHub.",
        "ppa:webupd8team/atom"]
}
```

## Usage
```bash
./deb-install.py
```

There is also the possibility to use your own .json file.
```bash
./deb-install.py ~/path/to/my.json
```
