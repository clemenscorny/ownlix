#!/usr/bin/env bash

# install debian packages
cd deb-install
./deb-install.py
cd ..

# Change dconf settings of some programs
./set-dconf.sh
